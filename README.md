Notre volonté est de créer une réflexion collective sur des problématiques relatives à l’évolution des écoles d’art et des questions de société qui y sont liées.
Apporter une contribution au débat, de façon claire et constructive, par une parole identifiée, sur des points qui nous touchent. 
Nous n’avons pas pour vocation de représenter l’ensemble des étudiants des écoles d’art de France et d’ailleurs.
Nous parlons en notre nom propre, et chacun est libre de rejoindre les débats et d’y apporter sa contribution. 

Participants:

Rémi Dufay; Victor Hamonic; Charlotte Nguyen; Pauline Creuzé; Florence Schmitt; Salômé Guillemin; Victor Le; 
Guillaume Vannier; Thiên Ngoc; Marion Bonjour; Arek Kouyoumdjian; Léa Arnaud; 

